from django.http import JsonResponse
from django.views import View

from django.conf import settings

FRAZ_HEADERS = getattr(settings, 'FRAZ_HEADERS')
FRAZ_API_KEY = getattr(settings, 'FRAZ_API_KEY')


class ServicePermissionMixin():
    def dispatch(self, request, *args, **kwargs):
        # api_key = request.headers.get('X-FRAZ-SERVICEAUTH')
        # can't use request.headers because of lower version of django
        api_key = request.META.get(
            f"HTTP_{FRAZ_HEADERS['service_auth_header']['meta_name']}"
        )
        if api_key == FRAZ_API_KEY:
            return super(ServicePermissionMixin, self).dispatch(request, *args, **kwargs)
        else:
            return JsonResponse({'error': 'Unauthorized access'}, status=401)


class FrazServiceView(
    ServicePermissionMixin,
    View
):
    pass
