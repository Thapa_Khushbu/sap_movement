import json

from django.db.models import F, Value
from django.shortcuts import render
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from apps.core.mixins import FrazServiceView
from apps.core.models import IN_PROGRESS, SAPMovementMock, NOT_PROCESSED, PROCESSED, FAILED
from django.utils import timezone

from .models import *


def home(request):
    return render(request, 'core/index.html')


class SAPInventoryGenAPI(FrazServiceView):
    http_method_names = ['get']

    def get(request, *args, **kwargs):
        machine_movements = SAPMovementMock.objects.filter(
            status='NOT_PROCESSED',
            show=True
        )

        response = list(
            machine_movements.values('id', 'job_number', 'serial_number', 'status', 'service_provider',
                                     'service_provider_sap', 'completion_date_sap'))
        counter_obj = SAPAPICounter.objects.first()
        if not counter_obj:
            SAPAPICounter.objects.create(count=1)
        else:
            counter_obj.count += 1
            counter_obj.save()
        # machine_movements.update(
        #     status=IN_PROGRESS
        # )
        # modify data as per need and add it in JsonResponse
        return JsonResponse({'detail': response})


class ConfirmationAPI(FrazServiceView):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ConfirmationAPI, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        # ids = request.data.get('processed_ids')
        # SAPMovementMock.objects.filter(
        #     id__in=ids
        # ).update(
        #     status=PROCESSED
        # )
        # SAPMovementMock.objects.filter(
        #     status=IN_PROGRESS
        # ).update(
        #     status=FAILED
        # )
        counter_obj = ConfirmAPICounter.objects.first()
        obj = SAPMovementMock.objects.first()
        request_body_json = request.body.decode('utf8').replace("'", '"')
        obj.request_dict = dict(
            request_obj=request.__dict__,
            request_body=request.body,
            request_post=request.POST,
            request_body_type=type(request.body),
            requst_body_json=request_body_json,
        )
        try:
            true_json = json.loads(request_body_json.split('"???",')[-1])
            obj.response_data_json = true_json
            json_list = true_json['response']
        except:
            json_list = []
            pass
        obj.save()
        for _response in json_list:
            obj = SAPMovementMock.objects.get(
                serial_number=_response.get('serial_number'),
                job_number=_response.get('job_number')
            )
            status = 'PROCESSED' if _response.get('action_result') == 'success' else 'FAILED'
            obj.status = status
            obj.sapb1_remarks = f"status: {status.lower()} at {timezone.now()}"
            obj.request_dict = _response.get('action_message')
            obj.save()
        if not counter_obj:
            SAPAPICounter.objects.create(count=1)
        else:
            counter_obj.count += 1
            counter_obj.save()
        return JsonResponse({'detail': 'Confirmation Complete!'})

    # issa hack hehe
    def get(self, request, *args, **kwargs):
        request.data = dict(processed_ids=[1])
        return self.post(request, *args, **kwargs)
