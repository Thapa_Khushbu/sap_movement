from django.urls import path

from .views import home, SAPInventoryGenAPI, ConfirmationAPI

urlpatterns = [
    path('', home, name='home'),
    path('get-data/', SAPInventoryGenAPI.as_view(), name='get-data'),
    path('confirm/', ConfirmationAPI.as_view(), name='confirm')
]
