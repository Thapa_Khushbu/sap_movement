from django.contrib import admin
from django.contrib.admin import register

from .models import SAPMovementMock, ConfirmAPICounter, SAPAPICounter

# Register your models here.
admin.site.register([ConfirmAPICounter, SAPAPICounter])


@register(SAPMovementMock)
class SAPMovementMockAdmin(admin.ModelAdmin):
    readonly_fields = ('service_provider_sap', 'completion_date_sap')
