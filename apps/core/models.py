import uuid
from django.db import models

# Create your models here.
from django.utils import timezone

from apps.core.constants import SERVICE_PROVIDERS, SERVICE_PROVIDERS_LIST, SERVICE_PROVIDERS_MAPPING

IN_PROGRESS = "IN PROGRESS"
PROCESSED = "PROCESSED"
NOT_PROCESSED = "NOT PROCESSED"
FAILED = "FAILED"


class SAPMovementMock(models.Model):
    SAP_STATUS_CHOICES = (
        ("PROCESSED", PROCESSED),
        ("IN_PROGRESS", IN_PROGRESS),
        ("NOT_PROCESSED", NOT_PROCESSED),
        ("FAILED", FAILED),
    )
    uuid = models.UUIDField(default=uuid.uuid4)
    completion_date = models.DateTimeField()
    job_number = models.CharField(max_length=200)
    service_provider = models.CharField(choices=SERVICE_PROVIDERS_LIST, max_length=200)
    service_provider_sap = models.CharField(null=True, blank=True, max_length=200)
    completion_date_sap = models.CharField(null=True, blank=True, max_length=200)
    serial_number = models.CharField(max_length=15)
    status = models.CharField(choices=SAP_STATUS_CHOICES,
                              help_text='SAP B1 database sync status',
                              null=True,
                              blank=True,
                              max_length=25)  # had to set null=True due to existing data
    sapb1_remarks = models.CharField(null=True, blank=True, max_length=200)
    show = models.BooleanField(default=True)
    request_dict = models.TextField(null=True, blank=True)
    response_data_json = models.TextField(null=True, blank=True)

    def save(self):
        try:
            print(self.service_provider)
            self.service_provider_sap = SERVICE_PROVIDERS_MAPPING[self.service_provider]
            self.completion_date_sap = self.completion_date.strftime('%Y-%m-%d')
        except:
            pass
        return super(SAPMovementMock, self).save()

    def __str__(self):
        return f"{self.serial_number}| status: {self.status}| show: {self.show}"


class SAPAPICounter(models.Model):
    count = models.PositiveIntegerField(default=0)
    last_updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"current SAP api hit count: {self.count}| Updated: {self.last_updated}"


class ConfirmAPICounter(models.Model):
    count = models.PositiveIntegerField(default=0)
    last_updated = models.DateTimeField()

    def __str__(self):
        return f"current 'Confirm' api hit count: {self.count}| Updated: {self.last_updated}"
